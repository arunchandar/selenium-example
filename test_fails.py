
# this test is set to fail!
# runs in both Firefox and Chrome
# runs as headless
# takes screenshot when fails
def test_python_dot_org(given_a_browser):
    browser = given_a_browser

    browser.get("http://www.python.org")
    assert "Java" in browser.title, "Assert set to fail so it takes the screenshot"
