import pytest
import re

from pages.pageobjet.HomePage import HomePage


@pytest.fixture()
def given_python_home_page(given_a_browser):
    driver = given_a_browser
    return HomePage(driver).open()


def when_click_on_pydocs(home_page):
    return home_page.click_pydocs()


def assert_page_title(pydocs_page):
    expected_text = r"<h1>Python .{5} documentation</h1>"
    assert re.search(expected_text, pydocs_page.driver.page_source)


@pytest.mark.nonheadless
def test_pydocs(given_python_home_page):
    python_home_page = given_python_home_page
    pydocs_page = when_click_on_pydocs(python_home_page)
    assert_page_title(pydocs_page)
