# Selenium example

To run:

```
> pipenv install
> pipenv run pytest
``` 

### Pre-requisites:
- Needs Python 3.9 and Pipenv.
- Chromedriver and Geckodriver are in the PATH.
