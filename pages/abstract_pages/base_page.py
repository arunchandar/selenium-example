from config import Config


class BasePage:

    def __init__(self, driver, relative_url=None):
        self.driver = driver
        self.base_url = Config.base_url
        self.relative_url = relative_url

    def get_url(self):
        url = "/".join([self.base_url, self.relative_url]) if self.relative_url else self.base_url
        return url

    def open(self):
        url = self.get_url()
        self.driver.get(url)
        return self

    def find(self, locator):
        return self.driver.find_element(*locator)

    def find_all_elements(self, locator):
        return self.driver.find_elements(*locator)

    def type_values(self, locator, text):
        text_field = self.find(locator)
        text_field.clear()
        text_field.send_keys(text)
        return self
