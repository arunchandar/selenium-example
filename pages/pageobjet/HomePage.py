from selenium.webdriver.common.by import By

from pages.abstract_pages.base_page import BasePage
from pages.pageobjet.PythonDocs import PythonDocs


class HomePage(BasePage):
    url_path = ""
    HOME = (By.XPATH, "//a[@title='The Python Programming Language']")
    DOCS = (By.XPATH, "//a[@title='Python Documentation']")
    PYPI = (By.XPATH, "//a[@title='Python Package Index']")
    JOBS = (By.XPATH, "//a[@title='Python Job Board']")
    SEARCH_FIELD = (By.ID, "id-search-field")
    SEARCH_BUTTON = (By.ID, "submit")

    def __init__(self, driver):
        super().__init__(driver)

    def open(self):
        super().open()
        return self

    def click_pydocs(self):
        docs_option = self.find(self.DOCS)
        docs_option.click()
        return PythonDocs(self.driver)
