import pytest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions
import datetime
import os

from config import Config


class BrowserFactory:

    def __init__(self, headless):
        self.headless = headless

    def get_chrome_options(self):
        options = ChromeOptions()
        prefs = {
            'profile.default_content_setting_values.automatic_downloads': 1,
            "download.prompt_for_download": False,
            "directory_upgrade": True,
        }
        options.add_experimental_option("prefs", prefs)
        options.add_argument("--window-size=1920,1080")

        if self.headless == "headless":
            options.add_argument("--headless")
        return options

    def get_firefox_options(self):
        options = FirefoxOptions()
        options.headless = True if self.headless == "headless" else False
        options.add_argument("--window-size=1920,1080")
        return options

    def create(self, browser_type):
        driver = {
            "chrome": lambda: webdriver.Chrome(options=self.get_chrome_options()),
            "firefox": lambda: webdriver.Firefox(options=self.get_firefox_options(),
                                                 service_log_path=os.devnull)
        }[browser_type]()

        driver.maximize_window()
        size = driver.get_window_size()
        width = max(size["width"], 1920)
        height = max(size["height"], 1080)
        driver.set_window_size(width, height)
        return driver


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep


def pytest_generate_tests(metafunc):
    """
    Clever pytest hook to inject different fixture into test
    :param metafunc:
    :return:
    """
    test_markers = getattr(metafunc.function, "pytestmark", [])
    nonheadless_marker = next((marker for marker in test_markers if marker.name == "nonheadless"), None)
    headless = "headless" if (nonheadless_marker is None) else "nonheadless"

    fixtures = metafunc.fixturenames
    if 'headless' in fixtures:
        metafunc.parametrize("headless", [headless])

    if 'chrome' in fixtures:
        metafunc.parametrize("browser_type", ["chrome"])
    else:
        metafunc.parametrize("browser_type", ["chrome", "firefox"])


@pytest.fixture()
def given_a_browser(headless, browser_type, request):
    driver = BrowserFactory(headless).create(browser_type)
    try:
        def take_screenshot():
            file_name = "{}.png".format(datetime.datetime.utcnow()).replace(':', '')
            _path = os.path.join(Config.screenshot_folder, request.node.name.replace('[', '-').replace(']', ''), file_name)
            directory = os.path.dirname(_path)
            if not os.path.exists(directory):
                os.makedirs(directory)
            driver.get_screenshot_as_file(_path)

        driver.take_screenshot = take_screenshot

        yield driver
    finally:
        if request.node.rep_setup.failed or request.node.rep_call.failed:
            try:
                driver.take_screenshot()
            except:
                pass
        driver.quit()
